// Import the page's CSS. Webpack will know what to do with it.
import "../stylesheets/app.css";

// Import libraries we need.
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'

//import voting_artifacts from '../../build/contracts/TridentTransaction.json'
import TridentTransactions_artifacts from '../../build/contracts/TridentTransaction.json'
var amount = contract();
var Voting = contract(TridentTransaction_artifacts);
let candidates = {"Deepak": "candidate-1", "Muthu": "candidate-2", "Partha": "candidate-3"}

window.TransCount = function(sender){
  let candidateName = $("#candidate").val();
  try {
    $("#msg").html("Amount has been Transfered to trident")
    $("#candidate").val("");
    TridentTransaction.deployed().then(function(contractInstance) {
    contractInstance.TransCount(candidateName, {gas: 140000, from: web3.eth.accounts[0]}).then(function() {
        let div_id = candidates[candidateName];
        return contractInstance.totalAmountFor.call(candidateName).then(function(v) {
          $("#" + div_id).html(v.toString());
          $("#msg").html("");
        });
      });
    });
  } catch (err) {
    console.log(err);
  }
}

$( document ).ready(function() {
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source like Metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }
  TridentTransaction.setProvider(web3.currentProvider);
  let candidateNames = Object.keys(candidates);
  for (var i = 0; i < candidateNames.length; i++) {
    let name = candidateNames[i];
    Voting.deployed().then(function(contractInstance) {
      contractInstance.totalAmountFor.call(name).then(function(v) {
        $("#" + candidates[name]).html(v.toString());
      });
    })
  }
});
