pragma solidity ^0.4.16;
contract TridentTransaction {
  //unsigned int can store transaction count
  mapping (bytes32 => uint8) public TranSendCount;
  //stored in array of senderlist
  bytes32[] public SenderList;
  //unsigned int can store transaction amout
 // mapping (bytes32 => uint8) public TransaAmount;
  uint8 public TransaAmount;
  uint256 public Token;

  //pass array of senderlist for the transaction
  function TridentTransaction(bytes32[] SenderNames) public {
    SenderList = SenderNames;
  }

  //use total amount transaction for a sender
  function totalAmountFor(bytes32 sender) view public returns (uint8) {
    require(validSender(sender));
    return TranSendCount[sender];
  }

  //increment transaction count and transaction amount for the specified sender
  function TransCount(bytes32 sender) public returns (uint){
    require(validSender(sender));
    TranSendCount[sender] += 1;
    //Token=TranSendCount;
    TransaAmount += TransaAmount;
    return TransaAmount;
    //return TranSendCount;
  }

  //cheks validation of users
 function validSender(bytes32 sender) view public returns (bool) {
    for(uint i = 0; i < SenderList.length; i++) {
      if (SenderList[i] == sender) {
        return true;
      }
    }
    return false;
  }

  //getting the transaction amount
 function AmountReturn() public returns (uint){
     Token=TransaAmount;
 }
}
